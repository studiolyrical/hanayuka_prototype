﻿using Microsoft.Xna.Framework.Graphics;

namespace hanayuka_prototype
{
    public class TextureComponent
    {
        public string Sprite { get; set; }
        public int Columns { get; set; }
        public int Rows { get; set; }
    }
}
