﻿using Microsoft.Xna.Framework;

namespace hanayuka_prototype
{
    public class CommonComponent : IComponent
    {
        public string Name { get; set; } // Object's friendly name
        public Vector2 Position { get; set; }
    }
}
