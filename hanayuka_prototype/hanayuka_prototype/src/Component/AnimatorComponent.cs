﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace hanayuka_prototype
{
    public class AnimatorComponent : IComponent
    {
        public int TotalFrames { get; set; }
        public int CurrentFrame { get; set; }
        public float SampleRate { get; set; }
        public float T { get; set; }
        public float Accumulator { get; set; }
    }
}
