﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.IO;
using System.Collections.Generic;
/* ----------------------------------------------------------------------------------
 * Description:
 * ● Evil "Helper" class full of static functions for various operations. うふふっ！
 * 
 * Flow:
 * ● Just sits here holding functions, no one ever instantiate this class.
 * 
 * List of Functions:
 * ● hanayuka_prototype.HelperFunctions.StringToVector2(string)
 * ---------------------------------------------------------------------------------- */
namespace hanayuka_prototype
{
    public class HelperFunctions
    {
        // Take a string in Vector2(x,y) format, convert it to a real Vector2, and return
        public static Vector2 StringToVector2(string s)
        {
            string[] splitter = s.Substring(0, s.Length - 1).Split(',');
            float x = float.Parse(splitter[0]);
            float y = float.Parse(splitter[1]);
            Vector2 vector2 = new Vector2(x, y);
            return vector2;
        }
    }
}
