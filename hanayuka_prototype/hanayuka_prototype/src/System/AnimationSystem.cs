﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
/* ----------------------------------------------------------------------------------
 * ● AnimationSystem, document soon
 * ---------------------------------------------------------------------------------- */
namespace hanayuka_prototype
{
    public class AnimationSystem : ISystem, IFixedUpdatable, IDrawable
    {
        /* ----------------------------------------------------------------------------------
         * FIELDS & PROPERTIES
         * ---------------------------------------------------------------------------------- */
        // Field for ghetto sample rate loop
        private const float DELTA_TIME = 100.0f;


        /* ----------------------------------------------------------------------------------
         * FUNCTIONS & STUFF
         * ---------------------------------------------------------------------------------- */
        public void FixedUpdate()
        {
            FrameUpdate();
        }

        /* ----------------------------------------------------------------------------------
         * ● FrameUpdate() runs at an arbitrarily defined "sample rate" to define the speed
         * of an animation.
         * ---------------------------------------------------------------------------------- */
        private void FrameUpdate()
        {
            foreach (GameObject go in GameObjectManager.ActiveObjects)
            {
                go.Animator.Accumulator += go.Animator.SampleRate;
                while (go.Animator.Accumulator >= DELTA_TIME) // Step through frames based on SampleRate
                {
                    go.Animator.CurrentFrame++; // Go to next frame
                    if (go.Animator.CurrentFrame == go.Animator.TotalFrames)
                        go.Animator.CurrentFrame = 0;
                    go.Animator.T += DELTA_TIME;
                    go.Animator.Accumulator -= DELTA_TIME;
                }
            }
        }

        public void Draw()
        {
            foreach (GameObject go in GameObjectManager.ActiveObjects)
            {
                // ○Texture ○Animator
                if (go.Animator != null && go.Texture != null)
                {
                    Texture2D tex = Hanayuka.spriteContent[go.Texture.Sprite];
                    // Lifted from rbwhitaker: http://rbwhitaker.wikidot.com/texture-atlases-2
                    int width = tex.Width / go.Texture.Columns;
                    int height = tex.Height / go.Texture.Rows;
                    int row = (int)((float)go.Animator.CurrentFrame / (float)go.Texture.Columns);
                    int column = go.Animator.CurrentFrame % go.Texture.Columns;

                    Rectangle sourceRectangle = new Rectangle(width * column, height * row, width, height);
                    Rectangle destinationRectangle = new Rectangle((int)go.C.Position.X, (int)go.C.Position.Y, width, height);
                    Hanayuka.spriteBatchReference.Draw(tex, destinationRectangle, sourceRectangle, Color.White);
                }
                // ○Texture ｘAnimator
                else if (go.Texture != null && go.Animator == null)
                {
                    Texture2D tex = Hanayuka.spriteContent[go.Texture.Sprite];
                    // Draw the texture without any animation
                    Hanayuka.spriteBatchReference.Draw(tex, go.C.Position, Color.White);
                }
            }
        }
    }
}
