﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;
/* ----------------------------------------------------------------------------------
 * Description:
 * ● Game1.cs
 * ---------------------------------------------------------------------------------- */
namespace hanayuka_prototype
{
    // This is the main type for the game
    public class Hanayuka : Microsoft.Xna.Framework.Game
    {
        /* ----------------------------------------------------------------------------------
         * CLASS DECLARATION
         * ---------------------------------------------------------------------------------- */
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        World world;
        Camera camera;

        /* ----------------------------------------------------------------------------------
         * FIELDS & PROPERTIES
         * ---------------------------------------------------------------------------------- */
        public static bool quit;
        private float scale; // Set by LoadContent() for screen/world scaling
        public static SpriteBatch spriteBatchReference; // public static reference to spriteBatch for ghetto animatorcomponent calls
        public static Dictionary<string, Texture2D> spriteContent;

        // Fields for Fixed timestep loop
        private static double m_totalFixedTime = 0.0;
        private double m_accumulator = 0.0;
        private const byte DELTA_TIME = 5;

        // ----------------------------------------- TEMP CODE -----------------------------------------
        Microsoft.Xna.Framework.Input.KeyboardState oldkbstate;
        // ----------------------------------------- TEMP CODE -----------------------------------------

        /* ----------------------------------------------------------------------------------
         * TEMP STUFF FOR GRAPHICS
         * ---------------------------------------------------------------------------------- */
        public static Texture2D yukariIdle;
        //Texture2D bg;
        //private AnimatorComponent bg_animator;

        /* ----------------------------------------------------------------------------------
         * CONSTRUCTOR
         * ---------------------------------------------------------------------------------- */
        public Hanayuka()
        {
            IsFixedTimeStep = false;
            quit = false;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            world = new World(); // temp world reference while I figure out watsanXNA?
            camera = new Camera();

            Components.Add(new FrameRateCounter(this, new Vector2(25, 25), Color.White, Color.Black));
        }

        /* ----------------------------------------------------------------------------------
         * FUNCTIONS AND STUFF
         * ---------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------
         * ● Allows the game to perform any initialization it needs to before starting to run.
         * This is where it can query for any required services and load any non-graphic
         * related content.  Calling base.Initialize will enumerate through any components
         * and initialize them as well.
         * ---------------------------------------------------------------------------------- */
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.SynchronizeWithVerticalRetrace = false;
            graphics.ApplyChanges();
            base.Initialize();
        }

        /* ----------------------------------------------------------------------------------
         * ● LoadContent will be called once per game and is the place to loadall of your 
         * content.
         * ---------------------------------------------------------------------------------- */
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteBatchReference = spriteBatch;
            spriteContent = TextureContent.LoadListContent<Texture2D>(Content, "Textures");
            //yukariIdle = Content.Load<Texture2D>("Textures/wat");
            //bg = Content.Load<Texture2D>("Textures/Stages/Battle/BattleBackground");
            //bg_animator = new AnimatorComponent();
            //bg_animator.SetAnimatorValues(bg, 1, 1, 0, 0f);
            //yukariIdle_animator = new AnimatorComponent(yukariIdle, 6, 6, 32, 13f);

            scale = (float)graphics.GraphicsDevice.Viewport.Width / 1280.0f;


            // TODO: use this.Content to load your game content here
        }

        /* ----------------------------------------------------------------------------------
         * ● UnloadContent will be called once per game and is the place to unload all content
         * ---------------------------------------------------------------------------------- */
        protected override void UnloadContent()
        {
            Content.Unload();
        }

        /* ----------------------------------------------------------------------------------
         * ● Update is run as fast as possible since 「IsFixedTimeStep = false;」
         * ● Inside of Update we want to handle things like Input. For game logic we use a 
         * subloop, which we'll call "FixedUpdate", which runs every 「DELTA_TIME」.
         * ● <param name="gameTime">Provides a snapshot of timing values.</param>
         * ---------------------------------------------------------------------------------- */
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            // ZA・WARUDO manages objects, and calls their respective update method in world.Update()
            world.Update();

            // Check to see if we should quit the game based on the static quit bool
            if (quit)
            {
                this.Exit();
            }

            /* ----------------------------------------------------------------------------------
             * ● Below while loop is the "FixedUpdate" logic:
             * ● http://gafferongames.com/game-physics/fix-your-timestep/
             * ---------------------------------------------------------------------------------- */
            m_accumulator += gameTime.ElapsedGameTime.TotalMilliseconds;
            while (m_accumulator >= DELTA_TIME) // Fixed timestep loop
            {
                //System.Console.WriteLine("elapsed game time is " + gameTime.ElapsedGameTime.TotalMilliseconds);
                //System.Console.WriteLine("accumulator is " + accumulator);
                world.FixedUpdate();
                m_totalFixedTime += DELTA_TIME;
                m_accumulator -= DELTA_TIME;
                //System.Console.WriteLine("total fixed time is " + m_totalFixedTime);
            }

            // ----------------------------------------- TEMP CODE -----------------------------------------
            Microsoft.Xna.Framework.Input.KeyboardState kbstate = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (oldkbstate.IsKeyUp(Microsoft.Xna.Framework.Input.Keys.Space) && kbstate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Space))
            {
                GameObjectManager gom = new GameObjectManager();
            }

            oldkbstate = kbstate;
            // ----------------------------------------- TEMP CODE -----------------------------------------

            base.Update(gameTime);
        }

        /* ----------------------------------------------------------------------------------
         * ● This is called when the game should draw itself (usually after every Update()).
         * ● More about calling update/draw here: 
         * http://blogs.msdn.com/b/shawnhar/archive/2007/11/23/game-timing-in-xna-game-studio-2-0.aspx
         * ---------------------------------------------------------------------------------- */
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            /* ----------------------------------------------------------------------------------
             * ● The matrix below translates from world space (using the 「camera」) to screen space.
             * ● This is primarily done to allow us to use arbitrary values for world space
             * coordinates, and translate things to any resolution proplerly.
             * ● 「scale」 is set in LoadContent().
             * ---------------------------------------------------------------------------------- */
            // Translate from world space to screen space
            Matrix mat = Matrix.CreateTranslation(camera.location.X, -camera.location.Y, 0) * Matrix.CreateScale(scale) * 
                         Matrix.CreateTranslation(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2, 1.0f);

            // After rewriting animation system, will create a proper manager for animations and have their shit set there; this is 
            // super ghetto way to handle animations until then.
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.AnisotropicClamp, null, null, null, mat);
            world.Draw();
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }

}
