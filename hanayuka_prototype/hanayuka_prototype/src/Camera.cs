﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
/* ----------------------------------------------------------------------------------
 * Description:
 * ● Right now this class just holds the camera variable, doing battle prototyping
 * so no logic at all. When it comes time to write overworld, will need to add
 * proper tracking logic.
 * ---------------------------------------------------------------------------------- */
namespace hanayuka_prototype
{
    public class Camera
    {
        public Vector2 location;
    }
}
