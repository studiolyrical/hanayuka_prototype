﻿//using Microsoft.Xna.Framework;
//using System;
//using System.Collections;

///* ----------------------------------------------------------------------------------
// * ● Typed Object for Actors
// * ---------------------------------------------------------------------------------- */
//namespace hanayuka_prototype
//{
//    public class ActorComponent : IActor, IUpdatable, IDrawable
//    {
//        /* ----------------------------------------------------------------------------------
//         * CLASS DECLARATION
//         * ---------------------------------------------------------------------------------- */

//        /* ----------------------------------------------------------------------------------
//         * FIELDS & PROPERTIES
//         * ---------------------------------------------------------------------------------- */
//        // Family-based Properties
//        public UniversalActor UniversalActorFamily { get; set; }
//        public PlayerActor PlayerActorFamily { get; set; }

//        // State
//        // just add another model here for concurrency
//        public ActorState state;
//        public Queue stateQueue = new Queue(); // add to this queue for pushdown automata


//        /* ----------------------------------------------------------------------------------
//         * CONSTRUCTOR
//         * ---------------------------------------------------------------------------------- */
//        public Actor()
//        {
//        }

//        /* ----------------------------------------------------------------------------------
//         * FUNCTIONS & STUFF
//         * ---------------------------------------------------------------------------------- */
//        public virtual void Update()
//        {
//        }

//        public virtual void FixedUpdate()
//        {
//            UniversalActorFamily.FixedUpdate();
//        }

//        public virtual void Draw()
//        {
//            UniversalActorFamily.Draw();
//        }
//    }
//}
