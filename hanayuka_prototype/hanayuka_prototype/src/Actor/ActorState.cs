﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//// this "state is classes" model can easily be expanded for hierarchal state machine

//namespace hanayuka_prototype
//{
//    public class ActorState : IActorState
//    {

//        public void handleInput(Actor actor)
//        {
//        }
//    }

//    class ActorIdleState : ActorState, IActorState
//    {
//        public void handleInput(Actor actor)
//        {
//        }
//    }

//    class ActorWalkingState : ActorState, IActorState
//    {
//        // Need to pass gamepad input state into this, ezpz though
//        public void handleInput(Actor actor)
//        {
//            if (true)
//            {
//                // move the animation to be "reactive" later
//                // Also switch it to a variable defined in the data file for the actor
//                //actor.UniversalActor.m_animator.SetAnimatorValues(Hanayuka.spriteContent["WalkForward"], 0, 0, 0, 0);
//                Console.WriteLine(actor + " is walking.");
//            }
//        }
//    }
//}
