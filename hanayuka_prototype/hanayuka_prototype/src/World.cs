﻿using System;
using System.Collections.Generic;
/* ----------------------------------------------------------------------------------
 * Description: 
 * ● ザ・ワールドっ！
 * ● The World class is 
 * ---------------------------------------------------------------------------------- */

namespace hanayuka_prototype
{
    public class World : IUpdatable, IFixedUpdatable, IDrawable
    {
        /* ----------------------------------------------------------------------------------
         * CLASS DECLARATION
         * ---------------------------------------------------------------------------------- */
        public GameObjectManager goManager;
        private AnimationSystem animationSystem;

        /* ----------------------------------------------------------------------------------
         * FIELDS & PROPERTIES
         * ---------------------------------------------------------------------------------- */
        // ----------------------------------------- TEMP CODE -----------------------------------------
        Microsoft.Xna.Framework.Input.KeyboardState oldkbstate;
        // ----------------------------------------- TEMP CODE -----------------------------------------


        /* ----------------------------------------------------------------------------------
         * CONSTRUCTOR
         * ● In this constructor, we create references to all the "Manager" classes and add them
         * to a list, 「managers」.
         * ---------------------------------------------------------------------------------- */
        public World()
        {
            goManager = new GameObjectManager();
            animationSystem = new AnimationSystem();
        }
        /* ----------------------------------------------------------------------------------
         * FUNCTIONS & STUFF
         * ---------------------------------------------------------------------------------- */
        // Call UpdateObjects on all systems
        public void Update()
        {
            // ----------------------------------------- TEMP CODE -----------------------------------------
            Microsoft.Xna.Framework.Input.KeyboardState kbstate = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (oldkbstate.IsKeyUp(Microsoft.Xna.Framework.Input.Keys.A) && kbstate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.A))
            {
                goManager.FetchObject(100);
            }

            oldkbstate = kbstate;
            // ----------------------------------------- TEMP CODE -----------------------------------------
        }

        // Call FixedUpdateObjects() on all systems
        public void FixedUpdate()
        {
            animationSystem.FixedUpdate();
        }

        // Call Draw() on all systems
        public void Draw()
        {
            animationSystem.Draw();
        }
    }
}
