﻿using System;
using System.Runtime.InteropServices;
/* ----------------------------------------------------------------------------------
 * Description:
 * ● Debug class for Lyrical Engine. Contains code for debugging ヽ(ﾟﾛﾟ；)
 * 
 * Flow: 
 * ● Usually the functions in here are static and we just call them as we need them.
 * 
 * List of Functions:
 * ● hanayuka_prototype.LyricalDebug.Assert(bool) 
 * ● hanayuka_prototype.LyricalDebug.MessageBox(System.IntPtr, string, string, uint)
 * ---------------------------------------------------------------------------------- */
namespace hanayuka_prototype
{
    public class LyricalDebug
    {
        // Grab user32.dll and hit up some message boxes
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern uint MessageBox(IntPtr hWnd, String text, String caption, uint type);

        /* ----------------------------------------------------------------------------------
         * ● Assert() method, Microsoft's isn't aggressive enough for me, I want the 
         * program to exit when an assertion fails.
         * ● http://en.wikipedia.org/wiki/Assertion_%28software_development%29
         * ---------------------------------------------------------------------------------- */
        public static void Assert(bool c)
        {
            if (!c)
            {
                Console.WriteLine(System.Environment.StackTrace);
                MessageBox(new IntPtr(0), System.Environment.StackTrace, "♥リリカル☆マジカル ASSERT FAILED!♥", 0);
                Hanayuka.quit = true;
                return;
            }
        }
    }
}
