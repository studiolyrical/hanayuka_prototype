﻿/* ----------------------------------------------------------------------------------
 * Description:
 * ● Container for components
 * ---------------------------------------------------------------------------------- */
namespace hanayuka_prototype
{
    public class GameObject
    {
        public CommonComponent C { get; set; }
        public TextureComponent Texture { get; set; }
        public AnimatorComponent Animator { get; set; }
    }
}
