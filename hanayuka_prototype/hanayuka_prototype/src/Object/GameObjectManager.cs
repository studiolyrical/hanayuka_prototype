﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
/* ----------------------------------------------------------------------------------
 * Description:
 * ● Manager for objects
 * ---------------------------------------------------------------------------------- */
namespace hanayuka_prototype
{
    public class GameObjectManager : IManager
    {
        /* ----------------------------------------------------------------------------------
         * INSTANCE VARIABLES
         * ---------------------------------------------------------------------------------- */
        // STATIC VARIABLES
        private static string m_objectData; // Data filepath for GameObjects
        private static Dictionary<int, GameObject> m_goMap; // Map of deserialized objects
        private static List<GameObject> m_activeObjects; // Keep track of all currently relevant objects
        private static bool instantiated = false; // Assertion check

        // PROPERTIES
        internal string ObjectData
        {
            get { return GameObjectManager.m_objectData; }
        }

        internal Dictionary<int, GameObject> GoMap
        {
            get { return GameObjectManager.m_goMap; }
        }

        internal static List<GameObject> ActiveObjects
        {
            get { return m_activeObjects; }
        }

        internal static bool Instantiated
        {
            get { return GameObjectManager.instantiated; }
        }

        /* ----------------------------------------------------------------------------------
         * CONSTRUCTOR
         * ---------------------------------------------------------------------------------- */
        public GameObjectManager()
        {
            LyricalDebug.Assert(!instantiated);
            m_objectData = System.IO.File.ReadAllText("data/object_data");
            m_goMap = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, GameObject>>(m_objectData);
            m_activeObjects = new List<GameObject>();
            instantiated = true;
        }

        /* ----------------------------------------------------------------------------------
         * FUNCTIONS
         * ---------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------
         * ● FetchObject() grabs an object from the map of deserialized JSON objects.
         * ● We add the GameObject to m_activeObjects, so that the rest of our game's logic
         * can do stuff with it.
         * ---------------------------------------------------------------------------------- */
        internal void FetchObject(int objID)
        {
            // Grab GameObject from map
            GameObject go = m_goMap[objID];

            // Add object to the active list
            m_activeObjects.Add(go);
        }
    }
}
