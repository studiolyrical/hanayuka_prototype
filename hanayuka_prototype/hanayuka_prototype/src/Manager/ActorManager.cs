﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework;
///* ----------------------------------------------------------------------------------
// * Description:
// * ● ActorManager fits into the Manager "pattern" that we're using for hanayuka. The
// * purpose of a Manager is to create, oversee, and destroy similar game objects. They
// * are sort of a mediator, and sort of a factory.
// * 
// * Flow:
// * ● When a new Actor should be made, it goes through CreateObject(). CreateObject
// * looks to data to create/setup a new actor object. Then the actor gets added to a 
// * list, and that list is iterated over to call every active Actor's main loop
// * methods.
// * ● The loop methods in THIS class get called by the World, which in turn gets called
// * by the main game loop.
// * 
// * List of Functions:
// * ● hanayuka_prototype.ActorManager.ActorManager() 
// * ● hanayuka_prototype.ActorManager.CreateObject(string) 
// * ● hanayuka_prototype.ActorManager.Draw() 
// * ● hanayuka_prototype.ActorManager.FixedUpdate() 
// * ● hanayuka_prototype.ActorManager.Update()
// * ---------------------------------------------------------------------------------- */
//namespace hanayuka_prototype
//{
//    public class ActorManager : IManager
//    {
//        /* ----------------------------------------------------------------------------------
//         * FIELDS & PROPERTIES
//         * ---------------------------------------------------------------------------------- */
//        private List<Actor> m_activeActors; // Keep track of all actors currently "alive"
//        private string m_actorData; // Actor data file, JSON

//        /* ----------------------------------------------------------------------------------
//         * CONSTRUCTORS
//         * ---------------------------------------------------------------------------------- */
//        public ActorManager()
//        {
//            m_activeActors = new List<Actor>();
//            m_actorData = System.IO.File.ReadAllText("data/actor_data");
//        }

//        /* ----------------------------------------------------------------------------------
//         * FUNCTIONS & STUFF
//         * ---------------------------------------------------------------------------------- */
//        // Call Update() on all active Actors
//        public void UpdateObjects()
//        {
//            foreach (Actor actor in m_activeActors)
//            {
//                actor.Update();
//            }

//            // Test code
//            if (GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed)
//            {
//                CreateObject("yukari");
//            }

//        }

//        // Call FixedUpdate() on all active Actors
//        public void FixedUpdateObjects()
//        {
//            foreach (Actor actor in m_activeActors)
//            {
//                actor.FixedUpdate();
//            }
//        }

//        // Call Draw() on all active Actors
//        public void DrawObjects()
//        {
//            foreach (Actor actor in m_activeActors)
//            {
//                actor.Draw();
//            }
//        }

//        // Generate a new Actor
//        public void CreateObject(string actorName)
//        {
//            // Create actor using data
//            Dictionary<string, Actor> actorMap = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<String, Actor>>(m_actorData);
//            Actor actorObj = actorMap[actorName];

//            // Add her to a list after being created
//            m_activeActors.Add(actorObj);
//        }
//    }
//}
