namespace hanayuka_prototype
{
#if WINDOWS || XBOX
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Hanayuka game = new Hanayuka())
            {
                game.Run();
            }
        }
    }
#endif
}

